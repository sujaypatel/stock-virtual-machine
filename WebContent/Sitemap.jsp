<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Site Map</title>
<link rel="stylesheet" href="css/main.css">
<style type="text/css">
.center {
    margin-left: 200px;
    margin-right: auto;
    width: 90%;
}

#navlist1 li {
	display: inline;
	list-style-type: none;
	text-align: left;
	font-size: 18px;
	padding: 0px 50px 0px;
}
</style>

</head>
<body class="LoginBackGround-Image2">

	<label class="margin-bottom-15"> <b></font><span
			style="color: Pink;"><font size="5"><center>
						SITE MAP</center></font> </span></b>
		<div id="navcontainer">
			<br>
			<ul id="navlist1">
				<li><a href="Introduction.jsp">INTRODUCTION</a></li>
				<li><a href="Legend.jsp">LEGEND</a></li>
				<li id="active"><a href="Sitemap.jsp" id="current">SITEMAP</a></li>
			</ul>
			<br>
		</div>
		<div class="center">
			<img src="images/sitemap.jpg" width="800" height="550" alt="Planets"
				usemap="#planetmap">

			<map name="planetmap">
				<area shape="rect" coords="340,56,456,141" href="Welcome.jsp" alt="" target="_top">
				<area shape="rect" coords="10,161,129,246" href="Login.jsp" alt="" target="_top">
				<area shape="rect" coords="152,169,258,248" href="Register.jsp" alt="" target="_top">
				<area shape="rect" coords="280,168,386,247" href="Portfolio.jsp" alt="" target="_top">
				<area shape="rect" coords="532,170,638,249" href="Contact.jsp" alt="" target="_top">
				<area shape="rect" coords="666,170,759,246" href="Help.jsp" alt="" target="_top">
			</map>
			
		</div>
	</label>
	


</body>
</html>